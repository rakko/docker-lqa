FROM debian:stretch-slim
RUN apt-get update && \
    apt-get install -y \
    python3 python3-pip python3-setuptools \
    git
RUN pip3 install pyyaml
RUN git clone --depth=1 https://gitlab.collabora.com/collabora/lqa.git
WORKDIR lqa
RUN python3 setup.py install
WORKDIR /
ENTRYPOINT [ "/usr/local/bin/lqa" ]
ONBUILD ENTRYPOINT []
